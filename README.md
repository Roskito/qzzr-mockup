QZZR Mockup

I created this project using [React](https://facebook.github.io/react/).

I used [create-react-app](https://github.com/facebookincubator/create-react-app).

I built the mockup and pushed it live for you to view without having to run build. [ethandaley.com](http://www.ethandaley.com).

Thank you for taking the time to look over my work, please let me know if you have any questions.